# Easter Egg Hunt

By: Carlos Carlos Adan Cortes De la Fuente

Date: 16/02/2018

Version: 1.0

### Summary 
Battleship Web Game themed as Easter Egg Hunt
The goal of the game is to find all the missing easter eggs
This game can be played with a friend.

### TBA
- Improve the Two Player logic
- Make hot seat version, were each player places the eggs for the other player

### How to use
--------
- Download the source code
- Open index.html file preferably using a local server development environment such as XAMPP

Tip:
In the CSS file LOOK for a "TODO" that indicates to uncomment the .test class to reveal the eggs positioning